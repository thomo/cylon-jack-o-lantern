// interrupt service program
#include <htc.h>
#include "global.h"

void interrupt my_isr(void)
{
	int i;
	
	// only Timer0 Interrupt enabled
	T0IF = 0;   // clear interrupt flag
	TMR0 = TMR0_LOAD;

	++isr_loopcnt;

	if (isr_loopcnt >= LOOP_OFF) 
	{	
		isr_loopcnt = LOOP_START;
	}

	switch (isr_loopcnt) 
	{ 
		case LOOP_LIGHT: 
		{
			isr_port.data = 0x0;
			for(i = 0; i < 16; ++i)
			{
				if (leds[i] == LED_LIGHT)
				{
					bitset(isr_port.data, i);
				}
			}
			PORTC = isr_port.port[0];
			PORTB = isr_port.port[1];
			break;
		} 
		case LOOP_MIDDLE:
		{
			for(i = 0; i < 16; ++i)
			{
				if (leds[i] == LED_MIDDLE)
				{
					bitset(isr_port.data, i);
				}
			}
			PORTC = isr_port.port[0];
			PORTB = isr_port.port[1];
			break;
		} 
		case LOOP_DARK:
		{
			for(i = 0; i < 16; ++i)
			{
				if (leds[i] == LED_DARK)
				{
					bitset(isr_port.data, i);
				}
			}
			PORTC = isr_port.port[0];
			PORTB = isr_port.port[1];
			break;
		} 
	}
	
		
}

