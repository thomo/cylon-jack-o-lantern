//
// Processor 16F876A
//

#define _XTAL_FREQ 12000000.0

#include <htc.h>

// high speed crystal
// do not protect memory
// brown-out reset disabled
// power-up timer enabled
// watchdog timer disabled
// low voltage programming disabled

__CONFIG(HS & UNPROTECT & BORDIS & PWRTEN & WDTDIS & LVPDIS);

#include "global.h"

void led2(char *line)
{
	int i;
	for(i = 0; i < 16; ++i)
	{
		leds[i] = line[i];
	}
}

void small_delay(void)
{
	int i;
	for(i = 0; i<2; ++i)
	{
   		__delay_ms(50);
	}
}

void middle_delay(void)
{
	int i;
	for(i = 0; i<10; ++i)
	{
   		__delay_ms(50);
	}
}

void power_up_mode(void)
{
	int i;
	char *line;

	for(i = 0; i < 8; ++i)
	{
		line = power_up_sequence[i];
		led2(line);
		middle_delay();
	}
}

void scan_mode(void)
{
	int i;
	char *line;

	for(i = 0; i < 16; ++i)
	{
		line = scan_sequence[i];
		led2(line);
		middle_delay();
	}
	for(i = 15; i != 0; --i)
	{
		line = scan_sequence[i];
		led2(line);
		middle_delay();
	}
}

void standby_mode(void)
{
	int i;
	char *line;

	for(i = 0; i < 16; ++i)
	{
		line = standby_sequence[i];
		led2(line);
		small_delay();
	}
	for(i = 15; i != 0; --i)
	{
		line = standby_sequence[i];
		led2(line);
		small_delay();
	}
}


void pause_mode(void)
{
	int i;

	led2(off_sequence); 

	for(i = 0; i < 20; ++i)
	{
		small_delay();
	}
}

void main(void)
{
	TRISB = 0x0;			// all output 	
	TRISC = 0x0;			// all output
	
	OPTION = OPTION_CONFIG; 

	TMR0 = TMR0_LOAD;  

	T0IF = 0;       // Reset TMR0 interrupt flag
	T0IE = 1;       // Enable TMR0 interrupt
	GIE  = 1;       // Enable Global interrupt
	
	// main loop	
	for(;;)
	{
		power_up_mode();
		standby_mode();
		scan_mode();
		pause_mode();
	}
}